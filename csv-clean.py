# -*- encoding: utf-8 -*-
import csv

from datetime import datetime
from decimal import Decimal


file_name = 'Hatherleigh Transaction report 2014.15.csv'
# file_name = 'Hatherleigh Transaction report 2013.14.csv'
result = []
total = Decimal()


with open(file_name) as f:
    count = 0
    c = csv.reader(f)
    for row in c:
        col1 = row[0]
        if not col1 in ('Total', 'Date'):
            count = count + 1
            cash = Decimal(row[5].strip('£'))
            total = total + cash
            result.append({
                'Date': col1,
                'Machine': row[1],
                'Location': row[2],
                'Tariff': row[3],
                'Description': row[4],
                'Cash': cash,
            })

for row in result[:10]:
    print('{}   {}   {}'.format(row['Date'], row['Tariff'], row['Cash']))

print('Count: {}    Total: {}'.format(count, total))

with open('2014-2015.csv', 'w', newline='') as f:
    c = csv.writer(f, dialect='excel')
    c.writerow(['Date', 'Tariff', 'Ticket', 'Cash'])
    for row in result:
        s = row['Date']
        # 17/04/2014 11:10,55B,2hr,1.00
        d = datetime.strptime(s, '%d/%m/%Y %H:%M')
        tariff = row['Tariff']
        ticket = ''
        if 'A' in tariff:
            ticket = '30min-1hr'
        elif 'B' in tariff:
            ticket = '2hr'
        elif 'C' in tariff:
            ticket = '3hr'
        elif 'D' in tariff:
            ticket = '4hr'
        elif 'E' in tariff:
            ticket = 'AllDay'
        else:
            raise ValueError("No tariff '{}'".format(tariff))
        data = [
            #s,
            # '2012-05-01 00:00:00'
            d.strftime('%Y-%m-%d %H:%M:00'),
            tariff,
            ticket,
            row['Cash'],
        ]
        c.writerow(data)
