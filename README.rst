Council - Hatherleigh Parking
*****************************

.. highlight:: python

::

  virtualenv --python=python3.4 venv-parking
  source venv-parking/bin/activate
  pip install --upgrade pip

  pip install -r requirements.txt

::

  # tidy the csv file
  python csv-clean.py && tail 2014-2015.csv

  # open the notebook
  ipython notebook

Open ``parking.ipynb``
